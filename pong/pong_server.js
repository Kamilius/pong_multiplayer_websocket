var express = require('express'), 
	app = express(),
	socket = require('socket.io');

app.configure(function() {
	app.use(express.static(__dirname + '/'));
});

app.get('/', function(req, res){
  	res.sendfile('pong/pong_client.html');
});

app.get('/socket.io.js', function(req, res){
  	res.sendfile('pong/socket.io.js');
});

var server = app.listen(8081);
var io = socket.listen(server);

var BOARD_SIZE = {
		width: 30,
		height: 90
	},
	BALL_SIZE = {
		width: 20,
		height: 20
	},
	PONG_SIZE = {
		width: 640,
		height: 480
	},
	PADDING = 20;

var playerOnePosition = 195,
	playerTwoPosition = 195,
	ballPosition = {
		x: 21,
		y: 21
	},
    ballMovementVector = {
		x: -6,
		y: 2
   	},
	playersConnected = 0,
	playersScore = {
		p1: 0,
		p2: 0
	};

var posRefreshInterval;

io.sockets.on('connection', function (socket) {
	if (playersConnected < 2) {
		playersConnected++;
		socket.emit('newPlayerConnection', { id: playersConnected, allowConnection: true});

		socket.on('playerInput', function (data) {
			switch(data.id) {
				case 1:
					playerOnePosition = data.position;
					break;
				case 2:
					playerTwoPosition = data.position;
					break;
			}
		});

		if (playersConnected == 2)
			io.sockets.emit('gameState', { begin: true });
	} else {
		socket.emit('newPlayerConnection', { id: 0, allowConnection: false });
	}
});
io.sockets.on('disconnect', function (socket) {
	playersConnected--;
	io.sockets.emit('playerDisconnected', {});
	clearInterval(posRefreshInterval);
});

function checkBallCollision() {
	var nextBallPosition = {
		x: ballPosition.x + ballMovementVector.x,
		y: ballPosition.y + ballMovementVector.y
	}

	var playerOneCollision = (nextBallPosition.x <= (PADDING + BOARD_SIZE.width)) &&
							  ((nextBallPosition.y >= playerOnePosition || nextBallPosition.y + BALL_SIZE.height >= playerOnePosition) && nextBallPosition.y <= playerOnePosition + BOARD_SIZE.height);
	var playerTwoCollision = ((nextBallPosition.x + BALL_SIZE.width) >= (PONG_SIZE.width - (BOARD_SIZE.width + PADDING))) && 
							  ((nextBallPosition.y >= playerTwoPosition || nextBallPosition.y + BALL_SIZE.height >= playerTwoPosition) && nextBallPosition.y <= playerTwoPosition + BOARD_SIZE.height);

	var pongTableCollision = nextBallPosition.x <= PADDING || 
							(nextBallPosition.x + BALL_SIZE.width) >= PONG_SIZE.width - PADDING ||
							nextBallPosition.y <= PADDING ||
							(nextBallPosition.y + BALL_SIZE.height) >= PONG_SIZE.height - PADDING;
	if (playerOneCollision)
		return 1;
	if (playerTwoCollision)
		return 2;
	if (pongTableCollision)
		return 3;
	return 0;
}

function changeBallMovementVector(collisionType) {
	var nextBallPosition = {
		x: ballPosition.x + ballMovementVector.x,
		y: ballPosition.y + ballMovementVector.y
	}

	switch(collisionType) {
		case 1:
			if((nextBallPosition.y > playerOnePosition && nextBallPosition.y < playerOnePosition + BOARD_SIZE / 2) ||
			   (nextBallPosition.y < playerOnePosition + BOARD_SIZE && nextBallPosition.y > playerOnePosition + BOARD_SIZE / 2)) {
			   	if (ballMovementVector.y >= 0) {
			   		ballMovementVector.y += 2;

			   	} else {
			   		ballMovementVector.y -= 2;
			   	}        					
			}
			ballMovementVector.x *= -1;
			break;
		case 2:
			if((nextBallPosition.y > playerTwoPosition && nextBallPosition.y < playerTwoPosition + BOARD_SIZE / 2) ||
			   (nextBallPosition.y < playerTwoPosition + BOARD_SIZE && nextBallPosition.y > playerTwoPosition + BOARD_SIZE / 2)) {
				if (ballMovementVector.y > 0) {
			   		ballMovementVector.y += 2;
			   	} else {
			   		ballMovementVector.y -= 2;
			   	}
			}
			ballMovementVector.x *= -1;
			break;
		case 3:
			//right bottom
			if(ballMovementVector.x > 0 && ballMovementVector.y > 0) {
				//right
				if(nextBallPosition.x + BALL_SIZE.width >= PONG_SIZE.width - PADDING) {
					ballMovementVector.x *= -1;
					playersScore.p1++;
				//bottom
				} else {
					ballMovementVector.y *= -1;
				}
				break;
			}
			//right top
			if(ballMovementVector.x > 0 && ballMovementVector.y < 0) {
				//right
				if(nextBallPosition.x + BALL_SIZE.width >= PONG_SIZE.width - PADDING) {
					ballMovementVector.x *= -1;
					playersScore.p1++;
				//top
				} else {
					ballMovementVector.y *= -1;
				}
				break;       					
			}
			//left top
			if(ballMovementVector.x < 0 && ballMovementVector.y < 0) {
				//left
				if(nextBallPosition.x <= PADDING) {
					ballMovementVector.x *= -1;
					playersScore.p2++;
				//top
				} else {
					ballMovementVector.y *= -1;
				}
				break;
			}
			//left bottom
			if(ballMovementVector.x < 0 && ballMovementVector.y > 0) {
				//left
				if(nextBallPosition.x <= PADDING)
					ballMovementVector.x *= -1;
				//bottom
				else
					ballMovementVector.y *= -1;
					playersScore.p2++;
				break;
			}
			break;
	}	
}

function refreshBallPoss() {
	var collisionType = checkBallCollision();
	if(collisionType === 0) {
		ballPosition.x += ballMovementVector.x;
		ballPosition.y += ballMovementVector.y;
	} else {
		changeBallMovementVector(collisionType);
		refreshBallPoss();
	}
}

function refreshPoss() {
	refreshBallPoss();
	io.sockets.emit('positionsRefresh', { playerOne: playerOnePosition, playerTwo: playerTwoPosition, ball: ballPosition });
}

posRefreshInterval = setInterval(refreshPoss, 45);